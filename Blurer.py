#!/usr/bin/python
import cv2
import numpy as np
import sys

'''
Based on Alistair Muldal code, which can be found here:
http://pastebin.com/sBsPX4Y7
'''


#main class
#==========
class Blurer:
    def __init__(self):
        filename = sys.argv[1]
        img = cv2.imread(filename)
        img = self.kovesi(img, 10)
        cv2.imshow('testing...', img)
        cv2.waitKey(0)
            

    def kovesi(self, image, iter, kappa = 20, gamma = 0.45, option = 1):   
        '''Kovesi algorithm for anisotropic diffusion'''
        
        #convert img to grayscale and initialize output img
        gray_img   = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray_img   = gray_img.astype("float32")
        output_img = gray_img.copy()

        #initializing internal variables
        deltaS = np.zeros_like(output_img)
        deltaE = deltaS.copy()
        NS = deltaS.copy()
        EW = deltaS.copy()
        gS = np.ones_like(output_img)
        gE = gS.copy()

        for i in range(iter):
            deltaS[:-1, :] = np.diff(output_img, axis=0)
            deltaE[:, :-1] = np.diff(output_img, axis=1)

            #calculating the coefficients
            gS = np.exp(-(deltaS/kappa)**2.0)
            gE = np.exp(-(deltaS/kappa)**2.0)

            #updating matrices
            S = gS * deltaS
            E = gE * deltaE

            NS[:] = S
            EW[:] = E
            NS[1:, :] -= S[:-1, :]
            EW[:, 1:] -= E[:, :-1]
            output_img += gamma * (NS + EW)

        return output_img.astype("uint8")


#main program
#============
if __name__ == '__main__':
    app = Blurer()
