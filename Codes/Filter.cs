using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessingCore
{
    public static class Filter
    {
        private static double findMax(Image<Gray, byte> input)
        {
            double max = -1;
            for (int i = 0; i < input.Height; i++)
            {
                for (int j = 0; j < input.Width; j++)
                {
                    if (input.Data[i, j, 0] > max)
                    {
                        max = input.Data[i, j, 0];
                    }
                }
            }
            return max;
        }
        private static double[,] createGaussianMask(int windowSize, double sigma)
        {
            double[,] CWindow = new double[windowSize, windowSize];

            if (sigma == 0)
            {
                for (int i = 0; i < windowSize; i++)
                {
                    for (int j = 0; j < windowSize; j++)
                    {
                        CWindow[j, i] = 0.0;
                    }
                }
                CWindow[windowSize / 2, windowSize / 2] = 1;
                return CWindow;
            }


            double Sigma2 = 2.0 * sigma * sigma;

            for (int i = 0; i < windowSize; i++)
            {
                for (int j = 0; j < windowSize; j++)
                {

                    double distFromCenter = (Math.Sqrt((i - (windowSize / 2)) * (i - (windowSize / 2)) + (j - (windowSize / 2)) * (j - (windowSize / 2))));
                    double P1 = 1.0 / (Math.PI * Sigma2);
                    double P2 = -1.0 * (distFromCenter / Sigma2);
                    double P3 = Math.Exp(P2);
                    CWindow[j, i] =
                        (P1 * P3);
                }
            }
            double factor = 0;
            for (int i = 0; i < windowSize; i++)
            {
                for (int j = 0; j < windowSize; j++)
                {
                    factor += CWindow[j, i];
                }
            }
            if (factor == 0)
            {
                factor = 1;
            }
            else
            {
                for (int i = 0; i < windowSize; i++)
                {
                    for (int j = 0; j < windowSize; j++)
                    {
                        CWindow[j, i] /= factor;
                    }
                }
            }
            return CWindow;
        }
        private static Image<Gray, byte> applyConvolution(Image<Gray, byte> input, double[,] mask)
        {
            Image<Gray, byte> AuxImage = input.CopyBlank();
            int w = AuxImage.Width;
            int h = AuxImage.Height;
            int windowSize = mask.GetLength(0);
            windowSize /= 2;
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    double sum = 0;

                    for (int i = -windowSize; i <= windowSize; i++)
                    {
                        if (i + x < 0 || i + x >= w)
                        {
                            continue;
                        }
                        for (int j = -windowSize; j <= windowSize; j++)
                        {
                            if (j + y < 0 || j + y >= h)
                            {
                                continue;
                            }
                            else
                            {
                                sum += (input.Data[y + j, x + i, 0] * mask[j + windowSize, i + windowSize]);
                            }
                        }
                    }
                    AuxImage.Data[y, x, 0] = (byte)sum;
                }
            }
            return AuxImage;
        }
        public static Image<Gray, byte> anisotropicDiffusionFilter(string img, int windowSize = 25)
        {
            Image<Gray, byte> input = new Image<Gray, byte>(img);
            return anisotropicDiffusionFilter(input, windowSize);
        }
        public static Image<Gray, byte> anisotropicDiffusionFilter(Image<Gray, byte> input, int windowSize = 25)
        {
            Image<Gray, byte> inputResized = input.Resize(640, 480, Emgu.CV.CvEnum.Inter.Lanczos4);
            Image<Gray, byte> smooth = applyConvolution(inputResized, createGaussianMask(5, 1.6));

            smooth = applyConvolution(smooth.Canny(25, 25), createGaussianMask(3, 1));


            double maxSmooth = findMax(smooth);

            Image<Gray, byte> AuxImage = inputResized.CopyBlank();
            int w = AuxImage.Width;
            int h = AuxImage.Height;
            int halfWindowSize = windowSize / 2;
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    double sum = 0;
                    double[,] mask = createGaussianMask(windowSize, (maxSmooth - smooth.Data[y, x, 0]) / maxSmooth);

                    for (int i = -halfWindowSize; i <= halfWindowSize; i++)
                    {
                        if (i + x < 0 || i + x >= w)
                        {
                            continue;
                        }
                        for (int j = -halfWindowSize; j <= halfWindowSize; j++)
                        {
                            if (j + y < 0 || j + y >= h)
                            {
                                continue;
                            }
                            else
                            {
                                sum += (inputResized.Data[y + j, x + i, 0] * (mask[j + halfWindowSize, i + halfWindowSize]));
                            }
                        }
                    }
                    AuxImage.Data[y, x, 0] = (byte)sum;
                }
            }
            inputResized.Dispose();
            smooth.Dispose();
            return AuxImage;
        }

        /// <summary>
        /// Read http://www.peterkovesi.com/matlabfns/Spatial/anisodiff.m
        /// Or anisodiff_Kovesi.m
        /// </summary>
        public static Image<Gray, byte> anisoDiffKovesi(Image<Gray, byte> input, int niter, double kappa = 20, double lambda = 0.25, int option = 1)
        {
            Image<Gray, double> diff = input.Convert<Gray, double>();

            int rows = diff.Rows;
            int cols = diff.Cols;

            Matrix<double> ones = new Matrix<double>(cols, rows);
            ones.SetValue(1.0);

            for (int i = 0; i < niter; i++)
            {
                Image<Gray, double> dfifl = new Image<Gray, double>(cols + 2, rows + 2);
                diffl.ROI = new System.Drawing.Rectangle(1, 1, cols, rows);
                diff.CopyTo(diffl);

                diffl.ROI = new System.Drawing.Rectangle(1, 0, cols, rows);
                Image<Gray, double> deltaN = diffl.Sub(diff);

                diffl.ROI = new System.Drawing.Rectangle(1, 2, cols, rows);
                Image<Gray, double> deltaS = diffl.Sub(diff);

                diffl.ROI = new System.Drawing.Rectangle(2, 1, cols, rows);
                Image<Gray, double> deltaE = diffl.Sub(diff);

                diffl.ROI = new System.Drawing.Rectangle(0, 1, cols, rows);
                Image<Gray, double> deltaW = diffl.Sub(diff);

                diffl.Dispose();
                GC.Collect();

                Image<Gray, double> cN = null;
                Image<Gray, double> cS = null;
                Image<Gray, double> cE = null;
                Image<Gray, double> cW = null;

                cN = setCoefficient(deltaN, option, kappa);
                cS = setCoefficient(deltaS, option, kappa);
                cE = setCoefficient(deltaE, option, kappa);
                cW = setCoefficient(deltaW, option, kappa);

                cN._Mul(deltaN);
                cS._Mul(deltaS);
                cE._Mul(deltaE);
                cW._Mul(deltaW);

                deltaN.Dispose();
                deltaS.Dispose();
                deltaE.Dispose();
                deltaW.Dispose();

                cN._Mul(lambda);
                cS._Mul(lambda);
                cE._Mul(lambda);
                cW._Mul(lambda);

                somaInPlace(ref diff, cN);
                somaInPlace(ref diff, cS);
                somaInPlace(ref diff, cE);
                somaInPlace(ref diff, cW);


                cN.Dispose();
                cS.Dispose();
                cE.Dispose();
                cW.Dispose();

                GC.Collect();
            }

            return diff.Convert<Gray, byte>();
        }

        private static void somaInPlace(ref Image<Gray, double> input, Image<Gray, double> cD)
        {
            var auxSoma = input.Add(cD);
            input.Dispose();
            input = auxSoma;
        }

        private static Image<Gray, double> setCoefficient(Image<Gray, double> deltaD, int option, double kappa)
        {
            Image<Gray, double> cD = new Image<Gray, double>(deltaD.Cols, deltaD.Rows);
            if (option == 1)
            {
                var cDMulti = deltaD.Mul(1.0 / kappa);
                cD = cDMulti.Pow(2.0);
                cDMulti.Dispose();
                cD._Mul(-1.0);
                cDMulti = cD.Exp();
                cD.Dispose();
                return cDMulti;
            }
            else
            {
                var cDMulti = deltaD.Mul(1.0 / kappa);
                cD = cDMulti.Pow(2.0);
                cDMulti.Dispose();
                cDMulti = cD.Add(new Gray(1.0));
                cD.Dispose();
                cD = cDMulti.Pow(-1.0);
                cDMulti.Dispose();
                return cD;
            }
        }
    }
}
